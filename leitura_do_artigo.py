#!/usr/bin/env python
from datetime import datetime

from store_data import StoreData


class ExtrairLeituraArtigo:
    def __init__(self):
        self.leitura_do_artigo_list = list()

    def extrair_leitura_do_artigo_csv(self):
        print('Abrindo arquivo csv...')
        try:
            file = open('leitura_do_artigo.csv', encoding='utf-8')
            cabecalho_leitura_do_artigo = file.readline()
            print('Arquivo aberto com sucesso!')
            lista_dados = file.readlines()
            file.close()
            if lista_dados:
                for linha in lista_dados:
                    leitura_do_artigo = LeituraDoArtigo()
                    raw_data = linha.split("\n")[0].split(",")
                    leitura_do_artigo.id_do_artigo = raw_data[0]
                    leitura_do_artigo.paginas_lidas = int(raw_data[1])
                    leitura_do_artigo.terminei_em = datetime.strptime(raw_data[2],'%d/%m/%y')
                    leitura_do_artigo.fiz_resumo = True if raw_data[3] == '1' else False
                    self.leitura_do_artigo_list.append(leitura_do_artigo)
                print('Arquivo csv extraído com sucesso!')
            else:
                print('Não foram encontrados dados para LeituraDoArtigo no arquivo.')
            file_write = open('leitura_do_artigo.csv', 'w')
            file_write.write(cabecalho_leitura_do_artigo)
            file_write.close()
        except Exception as e:
            print('Extração não realizada. Erro: {erro}'.format(erro=e))


    def extrair_dados_leitura_do_artigo_banco_de_dados(self):
        with StoreData() as connection:
            leitura_do_artigo = connection.select_leitura_do_artigo()
            for linha in leitura_do_artigo:
                leitura_do_artigo = LeituraDoArtigo()
                leitura_do_artigo.id_do_artigo = linha[1]
                leitura_do_artigo.paginas_lidas = linha[2]
                leitura_do_artigo.terminei_em = linha[3]
                leitura_do_artigo.fiz_resumo = linha[4]
                self.leitura_do_artigo_list.append(leitura_do_artigo)


class LeituraDoArtigo:
    def __init__(self):
        self.id_do_artigo = None
        self.paginas_lidas = None
        self.terminei_em = None
        self.fiz_resumo = None

    def save(self):
        with StoreData() as connection:
            connection.insert_leitura_do_artigo(self)
