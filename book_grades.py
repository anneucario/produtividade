#!/usr/bin/env python

from store_data import StoreData


class ExtractBookGrades:
    def __init__(self):

        self.book_grades = None
        self.book_grades_list = list()

    def extract_book_grades_csv(self):
        try:
            file = open('book_grades.csv')
            print('Abrindo arquivo csv...')
            header_book_grades = file.readline()
            print('Arquivo csv aberto com sucesso!')
            lista_dados = file.readlines()
            file.close()
            if lista_dados:
                for line in lista_dados:
                    book_grades = BookGrades()
                    raw_data = line.split("/n")[0].split(",")
                    book_grades.id = raw_data[0]
                    book_grades.grades = int(raw_data[1])
                    self.book_grades_list.append(book_grades)
                print('Arquivo csv extraído com sucesso!')
            else:
                print('Não foram encontrados dados para BookGrades no arquivo.')
                file_write = open('book_grades.csv', 'w')
                file_write.write(header_book_grades)
                file_write.close()
        except Exception as e:
            print('Extração não realizada. Erro:{erro}'.format(erro=e))


    def extract_book_grades_data_base(self):
        with StoreData() as connection:
            book_grades = connection.select_book_grades()
            for line in book_grades():
                book_grades = BookGrades()
                book_grades.id = line[0]
                book_grades.grades = line[1]
                self.book_grades_list.append(book_grades)




class BookGrades:
    def __init__(self):

        self.id = None
        self.book_grades_id = None
        self.book_grades = None

    def save(self):
        with StoreData() as connection:
            connection.insert_book_grades(self)
