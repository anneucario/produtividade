#!/usr/bin/env python

from store_data import StoreData


class ExtrairCatalogoLivros:
    def __init__(self):

        self.catalogo_livros_list = list()

    def extrair_dados_livros_csv(self):
        print('Abrindo arquivos csv...')
        try:
            file = open('livro.csv', encoding='utf-8')
            cabecalho = file.readline()
            print('Arquivo csv aberto com sucesso!')
            lista_dados = file.readlines()
            file.close()
            if lista_dados:
                for linha in lista_dados:
                    catalogo_livros = CatalogoLivros()
                    raw_data = linha.split("\n")[0].split(";")
                    catalogo_livros.id_isbn = raw_data[0]
                    catalogo_livros.titulo = raw_data[1]
                    catalogo_livros.titulo_original = raw_data[2]
                    catalogo_livros.autoria = raw_data[3]
                    catalogo_livros.numero_de_paginas = int(raw_data[4])
                    catalogo_livros.genero_literario = raw_data[5]
                    catalogo_livros.edicao = raw_data[6]
                    catalogo_livros.ano_de_publicacao = int(raw_data[7])
                    catalogo_livros.editora = raw_data[8]
                    self.catalogo_livros_list.append(catalogo_livros)
                print('Arquivo extraído com sucesso!')
            else:
                print('Não foram encontrados dados para CatalogoLivros no arquivo.')
            file_write = open('livro.csv', 'w')
            file_write.write(cabecalho)
            file_write.close()
        except Exception as e:
            print('Extração não realizada. Erro:{erro}'.format(erro=e))

class CatalogoLivros:
    def __init__(self):
        self.id_isbn = None
        self.titulo = None
        self.titulo_original = None
        self.autoria = None
        self.numero_de_paginas = None
        self.genero_literario = None
        self.edicao = None
        self.ano_de_publicacao = None
        self.editora = None

    def save(self):
        with StoreData() as connection:
            connection.insert_catalogo_livros(self)

