#!/usr/bin/env python

import catalogo_livros as cl
import produtividade_escrita as pe
import produtividade_leitura as pl
from article_grades import ExtractArticleGrades
from artigos_cientificos import ExtrairArtigosCientificos
from book_grades import ExtractBookGrades
from leitura_do_artigo import ExtrairLeituraArtigo
from study_productivity import ExtractStudyProductivity
from work_productivity import ExtractWorkProductivity

class Commands:
    def __init__(self, comando):
        self.command = comando
        self.control()

    def control(self):
        if self.command == '1':
            self.inserir_produtividade()
        if self.command == '2':
            self.inserir_produtividade_leitura()
        if self.command == '3':
            self.inserir_catalogo_livros()
        if self.command == '4':
            self.inserir_artigos_cientificos()
        if self.command == '5':
            self.inserir_leitura_do_artigo()
        if self.command == '6':
            self.insert_article_grades()
        if self.command == '7':
            self.insert_book_grades()
        if self.command == '8':
            self.insert_study_productivity()
        if self.command == '9':
            self.insert_study_productivity()
        if self.command == '10':
            self.help()


    def help(self):
        text = '''
            Escreva no prompt os comandos desejados:
            1 -> Para inserir produtividade
            2 -> Para inserir catalogo de livros
            3 -> Para inserir produtividade de leitura
            4 -> Para inserir dados de artigos científicos
            5 -> Para inserir dados de leitura do artigo
            6 -> Para inserir nota do artigo
            7 -> Para inserir nota dos livros
            8 -> Para inserir produtividade de trabalho
            9 -> Para inserir produtividade de estudo
            10 -> Para pedir ajuda e conhecer os comandos disponíveis
        '''
        print(text)

    def inserir_produtividade(self):
        produtividade = pe.ExtrairDadosProdutividade()
        produtividade.extrair_dados_csv()
        for dados in produtividade.dados_produtividade_list:
            dados.save()


    def inserir_produtividade_leitura(self):
        produtividade_leitura = pl.ExtrairDadosLeitura()
        produtividade_leitura.extrair_dados_leitura_csv()
        for dados in produtividade_leitura.dados_leitura_list:
            dados.save()

    def inserir_catalogo_livros(self):
        catalogo_livros = cl.ExtrairCatalogoLivros()
        catalogo_livros.extrair_dados_livros_csv()
        for dados in catalogo_livros.catalogo_livros_list:
            dados.save()

    def inserir_artigos_cientificos(self):
        artigos_cientificos = ExtrairArtigosCientificos()
        artigos_cientificos.extrair_dados_artigos_cientificos_csv()
        for inserir_artigos_cientificos in artigos_cientificos.artigos_cientificos_list:
            inserir_artigos_cientificos.save()

    def inserir_leitura_do_artigo(self):
        leitura_do_artigo = ExtrairLeituraArtigo()
        leitura_do_artigo.extrair_leitura_do_artigo_csv()
        for inserir_leitura_do_artigo in leitura_do_artigo.leitura_do_artigo_list:
            inserir_leitura_do_artigo.save()

    def insert_article_grades(self):
        article_grades = ExtractArticleGrades()
        article_grades.extract_article_grades_csv()
        for insert_article_grades in article_grades.article_grades_list:
            insert_article_grades.save()

    def insert_book_grades(self):
        book_grades = ExtractBookGrades()
        book_grades.extract_book_grades_csv()
        for insert_book_grades in book_grades.book_grades_list:
            insert_book_grades.save()

    def insert_work_productivity(self):
        work_productivity = ExtractWorkProductivity()
        work_productivity.extract_work_productivity_csv()
        for insert_work_productivity in work_productivity.work_productivity_list:
            insert_work_productivity.save()

    def insert_study_productivity(self):
        study_productivity = ExtractStudyProductivity()
        study_productivity.extract_study_productivity_csv()
        for insert_study_productivity in study_productivity.study_productivity_list:
            insert_study_productivity.save()