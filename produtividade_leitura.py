#!/usr/bin/env python

from datetime import datetime, timedelta
from store_data import StoreData


class ExtrairDadosLeitura:
    def __init__(self):
        self.dados_leitura_list = list()

    def extrair_dados_leitura_csv(self):
        print('Abrindo arquivo csv...')
        try:
            file = open('leitura.csv')
            print('Arquivo csv aberto com sucesso!')
            cabecalho_leitura = file.readline()
            lista_dados = file.readlines()
            file.close()
            if lista_dados:
                for linha in lista_dados:
                    dados_leitura = DadosLeitura()
                    raw_data = linha.split("/n")[0].split(",")
                    dados_leitura.id_do_livro = raw_data[0]
                    dados_leitura.dia = datetime.strptime(raw_data[1],"%d/%m/%y")
                    dados_leitura.tempo_de_leitura = timedelta(minutes=int(raw_data[2]))
                    dados_leitura.paginas_lidas = int(raw_data[3])
                    dados_leitura.meta_do_dia = int(raw_data[4])
                    dados_leitura.terminei = True if raw_data[5] == '1' else False
                    dados_leitura.foi_resenhado_no_blog = True if raw_data[6] == '1' else False
                    self.dados_leitura_list.append(dados_leitura)
                print('Arquivo csv extraído com sucesso!')
            else:
                print('Não foi encontrado dados para leitura no arquivo.')
            file_write = open('leitura.csv', 'w')
            file_write.write(cabecalho_leitura)
            file_write.close()
        except Exception as e:
            print('Extração não realizada. Erro:{erro}'.format(erro=e))


    def extrair_dados_leitura_banco_de_dados(self):
        with StoreData() as connection:
            dados_leitura = connection.select_dados_leitura()
            for linha in dados_leitura:
                dados_leitura = DadosLeitura()
                dados_leitura.id_do_livro = linha[1]
                dados_leitura.dia = linha[2]
                dados_leitura.tempo_de_leitura = linha[3]
                dados_leitura.paginas_lidas = linha[4]
                dados_leitura.meta_do_dia = linha[5]
                dados_leitura.terminei = linha[6]
                dados_leitura.foi_resenhado_no_blog = linha[7]
                self.dados_leitura_list.append(dados_leitura)


class DadosLeitura:
    def __init__(self):
        self.id_do_livro = None
        self.dia = None
        self.tempo_de_leitura = None
        self.paginas_lidas = None
        self.meta_do_dia = None
        self.terminei = None
        self.foi_resenhado_no_blog = None

    def save(self):
        with StoreData() as connection:
            connection.insert_dados_leitura(self)

